import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main {

	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
		System.out.println("Hello world!");
		
		// Windows
	    //System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
		
		
		// 1. Configure Selenium to talk to Chrome
		System.setProperty("webdriver.chrome.driver","/Users/owner/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		
		// 2. Enter the website you want to go to
		String baseUrl = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";
		
		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);
	
		// 4. Enter a username
		// ---------------------
		// 4a. Find the box
		WebElement inputBox = driver.findElement(By.id("user-message"));
		// 4b. Put the input in there
		inputBox.sendKeys("here is some nonsense");

		// Get the submit button and click automatically
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
		// 5b. Press the button
		showMessageButton.click();
		
		
		Thread.sleep(1000);
		
		// 6. Press the login button
		// ---------------------
		// 6a. Find the login button
		WebElement loginButton = driver.findElement(By.id("loginbutton"));
		// 6b. Click the button
		loginButton.click();
		
		// 7. Close the browser
		Thread.sleep(2000);  //pause for 1 second before closing the browser
		driver.close();
		
		
		
		// get the actual value of the title
//		actualTitle = driver.getTitle();
//		System.out.println("Title of webpage: " + actualTitle);
		

				
		/*
		 * compare the actual title of the page with the expected one and print
		 * the result as "Passed" or "Failed"
		 */
//		if (actualTitle.contentEquals(expectedTitle)){
//		    System.out.println("Test Passed!");
//		} else {
//		    System.out.println("Test Failed");
//		    }
//		   
//		    //close Chrome
		   // driver.close();
		   
	}

}